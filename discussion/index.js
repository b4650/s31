const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoutes.js")
const app = express();
const port = 3000
app.use(express.json());
app.use(express.urlencoded({ extended:true }));
mongoose.connect("mongodb+srv://lriparip:Parexcellence.1@wdc028-course-booking.aaei5.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
	})
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.use("/tasks", taskRoute)


app.listen(port, () => console.log(`Now listening to port ${port}`));

