// contains all the endpoints for the applications; instead of putting a lot of routes in the index.js, we separate them in other file, routes.js (databaseRoutes.js i.e. taskRoutes.js)

const express = require("express");

//creates a router instances that functions as a ,odd;eware amd routing system; allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router()

//allows us to use the controllers functions
const taskController = require("../controllers/taskControllers.js")


router.get("/", (req, res) => {
	// taskController - a controller file that is existing in the repo
	//getAllTasks is a function that is encoded i9nside the taskController file
	// use.then to wait for the getAllTasks function to be executed before proceeding to the statements (res.send(result))
	taskController.getAllTasks().then(result => res.send(result))
})

router.post("/", (req,res) => {
	taskController.createTask(req.body).then(result => res.send(result))
})

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => 
		res.send(result))
	})

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => 
		res.send(result))
	})

router.get("/:id", (req, res) => {
	taskController.getById(req.params.id). then(result => res.send(result))
})

router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then (result =>
		res.send(result))
	})


module.exports = router;