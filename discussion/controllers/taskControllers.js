// containing the functions/statements/logic to be performed once a route has been triggered/entered
// responsible for execution of CRUD operations/methods.
// create controller functions that responds to routes

//to give access to the contents of the tasks for tasks.js in the models folder; meaning it can use the Task model
const Task = require("../models/task.js")

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error);
			return false
		}else{
			return savedTask
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error){
			console.log(error);
			return false
		}else{
			return removedTask
		}
	})
}

module.exports.updateTask = ( taskId, requestBody ) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			result.name = requestBody.name;
			return result.save().then((updateTask, error) => {
				if(error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}

// ACTIVITY

/*1 get a specific task in the database
    BUSINESS LOGIC
    "/:id"
    -find the id (findById())
    -return it as a response*/

module.exports.getById = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error)
			return false
		}else{
			return result
		}
	})
}


/*2. update the status of a task
    BUSINESS LOGIC
    "/:id/complete"
    - find the id of the task
    - change the value of the status into "complete" using the requestBody
    - save the task   */ 

module.exports.updateStatus = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error)
			return false
		}else {
			result.status = "complete";
			return result.save().then((updateStatus, error) => {
				if (error) {
					console.log (error)
					return false
				}else {
					return updateStatus
				}
			})
		}
	})
} 